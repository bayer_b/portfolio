#! /usr/bin/env python

import yfinance as yf
import datetime
import pandas as pd
import numpy as np
import matplotlib.pyplot as plt
import operator

def get_assets(tranzactions):
    return list(set([ tr['asset'] for tr in tranzactions ]))

def get_currency_ratio(ccy, base_ccy, start, end):
    if ccy == base_ccy:
        return 1
    
    symbol = "{}{}=X".format(ccy, base_ccy)
    return yf.Ticker(symbol).history(start=start, end=end)["Close"]

class Portfolio:
    def __init__(self, _assets, _start, _end, _base_ccy):
        self.assets = _assets
        self.start = _start
        self.end = _end
        self.base_ccy = _base_ccy
        self.data = None
    
    def fetch_data(self):
        cols = pd.MultiIndex.from_product([self.assets,['price', 'base_ccy_ratio', 'quantity', 'market_value_base_ccy']])
        self.data = pd.DataFrame(columns=cols)
        self.data.columns.names = ['', '']
        
        for asset in self.assets:
            tk = yf.Ticker(asset)
            self.data[asset, 'price'] = tk.history(start=self.start, end=self.end)["Close"]
            self.data[asset, 'base_ccy_ratio'] = get_currency_ratio(tk.info["currency"], self.base_ccy, self.start, self.end)
            self.data[asset, 'quantity'] = 0
    
    def add_tranzactions(self, tranzactions):
        for tr in tranzactions:
            self.data.loc[tr['when']:, (tr['asset'], 'quantity')] += tr['change']
        for asset in self.assets:
            self.data[asset, 'market_value_base_ccy'] = self.data[asset, 'price'] * self.data[asset, 'base_ccy_ratio'] * self.data[asset, 'quantity']
            
tranzactions = [
    {"when": datetime.datetime(2021,1,22), "change": +5, "asset": "DNN"},
    {"when": datetime.datetime(2021,3,23), "change": -3, "asset": "NXE"},
    {"when": datetime.datetime(2020,3,22), "change": +7, "asset": "FIND.V"}
]

tranzactions.sort(key=operator.itemgetter('when'))

pf = Portfolio(get_assets(tranzactions), datetime.datetime(2021,1,20), datetime.datetime(2021,1,29))
pf.fetch_data()
pf.add_tranzactions(tranzactions)

pf.data.plot.area(y=[i for i in list(pf.data.columns) if i[1] == 'market_value'])
plt.legend()
plt.ylabel('market value')

pf.data
