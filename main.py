#!/usr/bin/env python
# coding: utf-8

# In[40]:


get_ipython().run_line_magic('matplotlib', 'inline')
from pandas_datareader import data, wb
import pandas_datareader as pdr
import datetime
import matplotlib.pyplot as plt
from pandas.plotting import register_matplotlib_converters
register_matplotlib_converters()
import seaborn as sns
import numpy as np


# In[41]:


def get_stocks(tickers, start_date, end_date):
    return pdr.get_data_yahoo(tickers, start=start_date, end=end_date)


# In[42]:


def get_returns(stocks):
    daily_returns = stocks['Adj Close'].pct_change()
    monthly_returns = stocks['Adj Close'].resample('M').ffill().pct_change()
    cum_daily_returns=(daily_returns + 1).cumprod()
    cum_monthly_returns=(monthly_returns + 1).cumprod()
    return {
        'daily': stocks['Adj Close'].pct_change(),
        'monthly': stocks['Adj Close'].resample('M').ffill().pct_change(),
        'cum_daily': (daily_returns + 1).cumprod(),
        'cum_monthly': (monthly_returns + 1).cumprod()
    }


# In[45]:


def plot_corrs(returns):
    fig_corrs = plt.figure(figsize=(15,10))
    ax_daily_corrs = fig_corrs.add_subplot(221)
    ax_daily_corrs.set_title("daily correlations")
    ax_monthly_corrs = fig_corrs.add_subplot(222)
    ax_monthly_corrs.set_title("monthly correlations")
    
    sns.heatmap(returns['cum_daily'].corr(), annot=True, ax=ax_daily_corrs, cmap=sns.diverging_palette(220, 10, as_cmap=True))
    sns.heatmap(returns['cum_monthly'].corr(), annot=True, ax=ax_monthly_corrs, cmap=sns.diverging_palette(220, 10, as_cmap=True))
    
    plt.show()


# In[ ]:





# In[80]:


tickers = ["GLD", "SLV", "^GSPC", "EEM", "BTCUSD=X", "ETHUSD=X"]

stocks = get_stocks(tickers, datetime.datetime(2020, 7, 15),datetime.datetime.today())

returns = get_returns(stocks)


# In[81]:


plot_returns(returns)


# In[82]:


plot_corrs(rs)


# In[51]:


stocks


# In[74]:


plt.plot(stocks['Adj Close']['^GSPC'].rolling(10).std() )


# In[75]:


stocks[stocks.isna().any(axis=1)]


# In[76]:


stocks


# In[ ]:




